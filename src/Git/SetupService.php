<?php
namespace Frame\Module\Git;

class SetupService
{
	public $steps;
	public $stepIndex;
	protected $properly_setup = false;
	protected $current_step = null;
	protected $output = null;

	public function __construct(){
		$this->steps = [
			'rights' => 'cd ' . env('APP_PATH') . ' && git status 2>&1',
			'ssh' => 'ls -al '. env('APP_PATH') .'/data/git/.ssh 2>&1',
			'authentication' => 'cd '. env('APP_PATH') . ' && git pull 2>&1'
		];

		$this->ssh = [
			'folder' => env('APP_PATH') . '/data/git/.ssh',
			'private' => 'id_rsa',
			'public' => 'id_rsa.pub'
		];

		$this->handlers = [
			'ssh' => function(){ 
				$folder = env('APP_PATH') . '/data/git/.ssh';
				$config = array(
				    "digest_alg" => "sha512",
				    "private_key_bits" => 4096,
				    "private_key_type" => OPENSSL_KEYTYPE_RSA,
				);
				try{
					mkdir($this->ssh['folder'], 0755, true);

					// Create the private and public key
					$res = openssl_pkey_new($config);

					openssl_pkey_export($res, $privKey);

					// Extract the public key from $res to $pubKey
					$pubKey = openssl_pkey_get_details($res);
					$pubKey = $pubKey["key"];

					file_put_contents($this->ssh['folder'] . '/' . $this->ssh['private'], $privKey);
					file_put_contents($this->ssh['folder'] . '/' . $this->ssh['public'], $pubKey);
				}
				catch(Exception $e){
					return [ 'output' => $e->getMessage(), 'status' => 1 ];
				}

				/*
				mkdir(dirname($file), 0755);
				file_put_contents($file, '');*/
				return [ 'output' => '', 'status' => 0 ];
			}
		];
	}

	public function checkCurrentStep(){
		$this->stepIndex = 0;
		foreach($this->steps as $label => $cmd){
			$this->stepIndex++;
			$output = [];
    		exec($cmd, $output, $status);

    		if($status != 0){
    			$this->current_step = $label;
    			$this->output = $output;
    			break;
    		}
    	}

    	if($this->current_step === null){
    		$this->properly_setup = true;
    	}

    	return $this->properly_setup;
	}

	public function getCurrentStep(){
		return $this->current_step;
	}

	public function handle($step){
		if(array_key_exists($step, $this->handlers)){
			return $this->handlers[$step]();
		}
	}

	public function getPublicKey(){
		if(file_exists($this->ssh['folder'] . '/' . $this->ssh['public'])){
			return preg_replace('/\s+/', '', file_get_contents($this->ssh['folder'] . '/' . $this->ssh['public']));
		}
		return '';
	}
}
