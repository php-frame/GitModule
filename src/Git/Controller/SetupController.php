<?php
namespace Frame\Module\Git\Controller;

use Frame\Module\Admin\Controller\ModuleSettingsController;

use Frame\Module\Git\SetupService;

class SetupController extends ModuleSettingsController
{
	protected $file = '@Git/pages/setup.twig';

	public function get(){
		$this->args['setup'] = $this->request->getAttribute('SetupService');
		return parent::get();
	}

	public function post(){
		$data = $this->request->getAttribute('SetupService')->handle($this->args['step']);

		if($data['status'] != 0){
			$buildedMsg = '';
			foreach($data['output'] as $msg){
				$buildedMsg .= '<br>' . $msg;
			}
			$this->flashNow('error', $this->translator->lang('message.error') . $buildedMsg);
		}

		$this->request->getAttribute('SetupService')->checkCurrentStep();

		return $this->get();
	}
}
