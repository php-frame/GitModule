<?php
namespace Frame\Module\Git\Controller;

use Frame\Controller\Controller;

class PullController extends SettingsController
{
    public function post()
    {	

    	$this->args['output'] = shell_exec('cd .. && git pull 2>&1');

    	$this->flash('success', $this->translator->lang('@Git.action.pull.success'));

        return $this->get();
    }
}
