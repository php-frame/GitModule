<?php
namespace Frame\Module\Git\Controller;

use Frame\Controller\Controller;
use Frame\Module\Admin\Controller\ModuleSettingsController;

class SettingsController extends ModuleSettingsController
{
    protected $file = '@Git/pages/settings.twig';
}
