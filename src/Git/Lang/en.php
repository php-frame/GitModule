<?php

return [
    'action' => [
    	'pull' => [
    		'success' => 'Successfully pulled data from git'
    	]
    ],
    'messages' => [
    	'setup' => 'You have to setup properly your server before using these features'
    ],
    'setup' => [
    	'steps' => [
    		'rights' => [
    			'title' => 'Setting up rights',
    			'content' => 'You have to grant rights of git actions to your server user. The command to execute should be like : ',
                'done' => 'I\'ve done that'
    		],
    		'ssh' => [
    			'title' => 'Setting up authentication',
    			'notification' => 'There is two authentications modes with git. At the moment, the SSH mode is the only supported.',
    			'content' => 'You have to generate an SSH key for your server user.',
                'generate' => 'Generate an SSH key for me'
    		],
    		'authentication' => [
    			'title' => 'Add key to the project',
    			'content' => 'You have to go to your hosted repository and add the generated key to the allowed users. Use the following key :',
                'done' => 'I\'ve done that'
    		]
    	]
    ]
];