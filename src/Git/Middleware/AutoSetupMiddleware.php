<?php
namespace Frame\Module\Git\Middleware;

use Frame\Middleware\Middleware;

use Frame\Module\Git\SetupService;

class AutoSetupMiddleware extends Middleware
{
    public function __invoke($request, $response, $next)
    {
    	$setupService = new SetupService;
        $request = $request->withAttribute('SetupService', $setupService);

    	if(!$setupService->checkCurrentStep()){
    		if($request->getAttribute('route')->getName() != '@Git.setup'){
                $this->flash('warning', $this->translator->lang('@Git.messages.setup'));

    			return $this->redirect($response, '@Git.setup', [ 'step' => $setupService->getCurrentStep() ]);	
    		}
    	}

        $response = $next($request, $response);
        return $response;
    }
}
