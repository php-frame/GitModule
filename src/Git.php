<?php
namespace Frame\Module;

// Controllers
use Frame\Module\Git\Controller\PullController;
use Frame\Module\Git\Controller\SettingsController;
use Frame\Module\Git\Controller\SetupController;

// Middleware 
use Frame\Module\Admin\Middleware\AdminMiddleware;
use Frame\Module\Git\Middleware\AutoSetupMiddleware;

use Frame\Module\Git\Model\User;

class Git extends \Frame\Module
{
	const VIEWS = __DIR__ . '/Git/Ressources/views';
	const LANGUAGES = __DIR__ . '/Git/Lang';

    public function __invoke(){
    	$c = $this->app->getContainer();

		$this->app->group('/admin/git', function() use ($c){
			$this->get('', function($req, $res, $args){
	    		return $res->withRedirect($this->router->pathFor('@Git.overview'));
	    	})->setName('@Git.home');

			$this->group('/settings', function(){
		    	$this->map(['GET'], '', SettingsController::class)->setName('@Git.admin.settings');

		    	$this->map(['GET', 'POST'], '/setup/{step}', SetupController::class)->setName('@Git.setup');

				$this->map(['GET', 'POST'], '/pull', PullController::class)->setName('@Git.pull');
			})->add(AutoSetupMiddleware::class);
		})->add(AdminMiddleware::class);
    }
}